import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Link } from "react-router-dom";


function MoviesList() {
  const [films, setFilms] = useState(['blah',])
  useEffect(() => {
    axios.get('https://ghibliapi.herokuapp.com/films')
      .then(res => setFilms(res.data));
  }, []);
  const listItems = films.map((item, index) => (
    <li key={item.id} className={index % 2 !== 0 ? "odd" : ""}>
      <Link to={`/details/${item.id}`} className="name">
        {item.title}
      </Link>
      <span className="director">
        {item.director}
      </span>
      <span className="description">
        {item.description}
      </span>
    </li>
  ));
  return (
    <ul className="movieList">
      <li className="listHeader odd">
        <span>
          Movie Title
      </span>
        <span>
          Director
      </span>
        <span className="description">
          Description
      </span>
      </li>
      {listItems}
    </ul>
  );
}

export default MoviesList;
