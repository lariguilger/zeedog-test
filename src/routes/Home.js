import React from 'react';
import MoviesList from './../components/MoviesList';

function Home() {
  return (
    <div className="container">
      <div className="heading">
        <h2>Movies from Studio Ghibli</h2>
        <p>Click on a movie title for more details</p>
      </div>
      <div className="MoviesDiv">
        <MoviesList />
      </div>
    </div>
  );
}

export default Home;
