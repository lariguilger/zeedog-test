import React, { useEffect, useState } from 'react';
import axios from "axios";
import { useParams } from "react-router-dom";

function App() {
  const [details, setDetails] = useState(null)
  let { id } = useParams()
  useEffect(() => {
    axios.get(`https://ghibliapi.herokuapp.com/films/${id}`).then(res => setDetails(res.data));
  }, [id]);

  return (
    <div className="container">
      {
        details !== null ? (
          <div className="detailsRow">
            <div className="basicInfo">
              <h2>{details.title}</h2>
              <h3><strong>Directed by:</strong> {details.director}</h3>
              <h3><strong>Producer: </strong>{details.producer}</h3>
              <h3><strong>Release Date: </strong>{details.release_date}</h3>
            </div>
            <div className="detailsCol">
              <p>{details.description}</p>
              <h3>Rating in Rotten Tomatoes:</h3>
              <h2>{details.rt_score}/100</h2>
            </div>


          </div>
        ) : (null)
      }
    </div>
  );
}

export default App;
