import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import './App.css';
import Home from './routes/Home';
import Details from './routes/Details';

function App() {
  return (
    <Router>
      <div className="App">
        <NavLink
          to="/">
          <header className="appHeader">

          </header>
        </NavLink>
        <Switch>
          <Route path="/details/:id">
            <Details />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
